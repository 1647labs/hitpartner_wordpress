=== YITH WooCommerce Membership ===

== Changelog ==

= Version 1.3.5 - Released: Dec 27, 2017 =

* New - translate plan title with WPML
* Tweak - added gettext to print membership note to make it translatable through WPML String Translations
* Update - language file
* Fix - integration with YITH WooCommerce Dynamic Pricing and Discounts 1.4.2
* Fix - Linked Plans field issue
* Fix - WPML issue
* Dev - added yith_wcmbs_membership_get_plan_title filter
* Dev - fixed yith_wcmbs_get_restricted_items_in_plan filter issue

= Version 1.3.4 - Released: Dec 11, 2017 =

* Update - Plugin Framework 3.0
* Fix - register post type issue, since it was registered only if is_admin
* Fix - membership free shipping hooks
* Fix - multiple membership issue when creating new one manually

= Version 1.3.3 - Released: Oct 25, 2017 =

* New - support to WooCommerce 3.2.1
* New - integration with YITH WooCommerce Mailchimp 1.1.1: the admin can set Mailchimp lists in Membership plans, so when an user becomes a member he/she will be added to the selected Mailchimp lists
* Fix - YITH WooCommerce Multi Vendor integration
* Fix - removed possibility to create post for Membership plans
* Fix - products in membership
* Dev - added yith_wcmbs_add_products_in_plan_cat_tag_args filter
* Dev - added yith_wcmbs_product_is_in_plans filter
* Tweak - delete cron when uninstalling

= Version 1.3.2 - Released: Oct 11, 2017 =

* New - support to Support to WooCommerce 3.2.0 RC2
* Fix - wordpress version number in WP_Compatibility class
* Fix - prevent fatal error in combination with YITH WooCommerce Multi Vendor
* Fix - Plan Item Order scrollable issue
* Tweak - added CSS classes in membership information in My Account
* Tweak - fixed yith_wcmbs_hide_price_and_add_to_cart filter name
* Tweak - new shortcode tab style
* Tweak - changed the_content filter priority to prevent issues with other plugins
* Tweak - added do_shortcode for alternative contents
* Dev - added yith_wcmbs_frontend_js_deps filter
* Dev - added yith_wcmbs_email_membership_status_expiration_date filter
* Dev - added yith_wcmbs_my_account_membership_status_expiration_date filter
* Dev - added yith_wcmbs_email_membership_status_expiration_date filter
* Dev - added yith_wcmbs_membership_created action
* Dev - added yith_wcmbs_not_enough_credits_message filter
* Dev - added yith_wcmbs_membership_get_remaining_days filter
* Dev - added yith_wcmbs_get_product_download_links filter
* Dev - added yith_wcmbs_sorted_plan_items filter
* Dev - added yith_wcmbs_shortcode_membership_download_product_links_name filter
* Dev - added yith_wcmbs_shortcode_membership_download_product_links_link filter
* Dev - added yith_wcmbs_shortcode_membership_download_product_links_data filter

= Version 1.3.1 - Released: Mar 16, 2017 =

* New - support to WooCommerce 3.0.0-RC1
* New - added German translation (thanks to Ninos Ego)
* Dev - added yith_wcmbs_get_restricted_items_in_plan filter
* Dev - added yith_wcmbs_check_if_external_file_exists filter
* Dev - added yith_wcmbs_non_allowed_post_ids_for_user filter


= Version 1.3.0 - Released: Jan 02, 2017 =

* New - more than one membership target product in membership plans
* Fix - issues with formatted dates
* Fix - tip-tip in reports
* Fix - display date in localized format
* Fix - issue with access restrictions in the shop page
* Fix - check if linked file exists to prevent displaying the link
* Fix - added WP retro compatibility
* Tweak - added membership info column in "downloads by user" reports
* Tweak - "Credits" field moved to Advanced tab in product (for variable products)
* Tweak - improved subscription information in membership tip-tip (in combination with YITH WooCommerce Subscription Premium)
* Dev - added hooks in reports
* Dev - added action yith_wcmbs_download_report_after_graphics


= Version 1.2.9 - Released: Nov 07, 2016 =

* New - Spanish language
* New - new reports (downloads by user and download details by user)
* Fix - issue in Edit Plan when adding ajax select2
* Fix - membership history view
* Fix - reports style in order page
* Dev - added yith_wcmb_update_membership_status_allowed filter
* Dev - added yith_wcmb_allow_status_management_by_subscription filter
* Dev - added yith_wcbms_remove_woocommerce_product_shop_actions action
* Dev - added yith_wcbms_restore_woocommerce_product_shop_actions action
* Dev - added yith_wcbms_remove_woocommerce_product_actions action
* Dev - added yith_wcbms_restore_woocommerce_product_actions action

= Version 1.2.8 - Released: Sep 06, 2016 =

* New - user option in membership_protected_content shortcode to display content to non-members or guests or logged users only
* Fix - shortcode issue
* Fix - membership activation issue in combination with polylang

= Version 1.2.7 - Released: Jul 19, 2016 =

* Fix - download link style
* Fix - protected link saving

= Version 1.2.6 - Released: Jul 13, 2016 =

* New - protected links in posts, pages and product descriptions
* New - protected contents through shortcode
* New - "copy to clipboard" for shortcodes in Membership Plan list
* New - improved CSS and JS inclusion
* Fix - YITH WooCommerce Multi Vendor compatibility (hide alternative content if user is not enabled to see it)

= Version 1.2.5 - Released: Jun 16, 2016 =

* New - Membership Free Shipping method since WooCommerce 2.6
* New - parameter to sort membership items in [membership_items] shortcode
* New - italian language

= Version 1.2.4 - Released: May 31, 2016 =

* New - possibility to associate automatically membership plans to newly registered users

= Version 1.2.3 - Released: May 17, 2016 =

* New - compatibility with YITH WooCommerce Dynamic Pricing and Discounts 1.1.0 to allow discounts for members
* New - shortcode to show downloaded product links
* Fix - bug in memberships with user_id = 0
* Fix - issue with creation of download report table on multisite installation
* Fix - memory issue in Membership Plan settings
* Fix - membership access issue
* Tweak - fixed strings

= Version 1.2.2 - Released: Mar 15, 2016 =

* Tweak - display membership access in Media Editor
* Tweak - fixed Multi Vendor suborder bug ( duplicate membership )
* Tweak - fixed minor bugs

= Version 1.2.1 - Released: Mar 10, 2016 =

* New - possibility to change the subscription id for every membership with membership advanced management enabled
* New - order info in membership list
* New - sorting for starting and expiring date in All Memberships WP List
* Fix - datepicker css style
* Fix - date bug in advanced membership management
* Fix - redirect bug for pages in membership

= Version 1.2.0 - Released: Feb 25, 2016 =

* New - membership advanced management
* New - possibility to hide product download links and use shortcode
* New - credit advanced management, admin can set different credits for every product (default is 1)
* New - possibility to set credits for the first term
* New - compatibility with Premium YITH WooCommerce Email Templates 1.2.0
* New - possibility to override membership email templates
* New - subscription status in All Memberships list
* New - possibility to hide price and add-to-cart button in Single Product Page, if members are allowed to download the product
* New - reports for memberships purchased with subscription
* Fix - duplicate membership plan
* Fix - CSS tooltip in frontend
* Fix - Membership can now be activated even when cancelled Subscription is payed
* Fix - subscription cancel-now bug
* Fix - issue concering product download by admin (check credit error); now Admin doesn't need credits to download products
* Tweak - added hierarchical structure in "chosen for product" and post categories
* Tweak - added buttons "Select All" and "Deselect All" for chosen field of post and product categories in Membership Plan Options
* Tweak - added action to manage PayPal and Stripe disputes
* Tweak - improved frontend style for membership history, download buttons and list of planned items
* Tweak - added admin tab shortcodes to explain shortcode usage
* Tweak - email classes and templates updated for WC 2.5
* Tweak - improved reports
* Tweak - changed status label from "Not Active" to "Suspended"
* Tweak - changed labels in admin membership plan
* Tweak - included child categories for products if parent category is selected in Membership Plan Options
* Tweak - fixed css for metabox chosen, select and descriptions
* Tweak - added style for download buttons in Single Product Page

= Version 1.1.1 - Released: Jan 15, 2016 =

* Tweak - fixed bug for current memberships without credits management

= Version 1.1.0 - Released: Jan 13, 2016 =

* New - download credits management for membership
* New - possibility to choose limit for membership downloads
* New - membership and download reports
* New - user download reports table and graphics in orders
* New - status filters in Memberships WP List
* New - compatibility with WooCommerce 2.5 RC2
* Tweak - fixed minor bug with bbPress
* Tweak - fixed membership bulk actions for users
* Tweak - fixed pot language file
* Tweak - changed menu name Memberships in All Memberships
* Tweak - fixed minor bugs

= Version 1.0.4 - Released: Dec 07, 2015 =

* New - possibility to hide items directly in membership plan settings page
* Tweak - better styling management for membership item list (shortcode)
* Tweak - improved compatibility with YITH WooCommerce Multi Vendor
* Tweak - improved cron performance
* Tweak - fixed end date calculation after pause
* Tweak - fixed minor bugs

= Version 1.0.3 - Released: Dec 01, 2015 =

* New - support for membership bought by guest users
* New - shortcode for showing membership history
* Tweak - improved compatibility with YITH WooCommerce Multi Vendor
* Tweak - added possibility to hide membership history in My Account page
* Tweak - improved download list

= Version 1.0.2 - Released: Nov 17, 2015 =

* Initial release