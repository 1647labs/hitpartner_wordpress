��          �   %   �      p     q     �     �     �     �     �     �     �     �  
   �     �     �     �     �            
   :     E     Z     h     o     t  ;   y  >   �  "   �          (  �  <     �     �     �     �       	             !     &     .     >     U  	   Z  
   d  	   o  $   y     �     �     �     �     �     �  <   �  =   %  '   c     �  	   �                       	                                                                              
                      %s [Order not found] Contents Description Deselect All Error Examples Expiration Date Guest History Login Form Membership Plans: Name Note Order Order number by X%s by %s Print WooCommerce Login Form Select All Shortcode attributes Starting Date Status Type User You don't have any membership with a subscription plan yet. You don't have any membership without a subscription plan yet. You don't have any membership yet. created by Admin full name%1$s %2$s Project-Id-Version: YITH WooCommerce Membership
POT-Creation-Date: 2016-12-16 11:11+0100
PO-Revision-Date: 2017-02-01 10:34+0100
Last-Translator: Ninos Ego <me@ninosego.de>
Language-Team: Yithemes <plugins@yithemes.com>
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.11
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;__ngettext:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=n!=1;
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: plugin-fw
 %s [Bestellung nicht gefunden] Inhalte Beschreibung Alle abwählen Fehler Beispiele Ablaufdatum Gast Verlauf Anmeldeformular Mitgliedschaftspläne: Name Bemerkung Bestellung %s von %s WooCommerce Anmeldeformular ausgeben Alle auswählen Shortcode Eigenschaften Anfangsdatum Status Type Benutzer Du hast derzeit keine Mitgliedschaften mit einem Abonnement. Du hast derzeit keine Mitgliedschaften ohne einem Abonnement. Du hast derzeit keine Mitgliedschaften. von Admin erstellt %1$s %2$s 